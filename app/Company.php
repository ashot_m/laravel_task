<?php

namespace App;

use App\Employee;
use Illuminate\Database\Eloquent\Model;


class Company extends Model
{
    public $timestamps = true;

    protected $table = 'companies';

    protected $fillable = [
        'name', 'email', 'logo', 'website'
    ];

    public function employees()
    {
        return $this->hasMany(Employee::class, 'company_id');
    }

    public function CompanyImages()
    {
        return $this->hasMany(CompanyImage::class);
    }

}
