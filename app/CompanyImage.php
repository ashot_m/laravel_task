<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyImage extends Model
{
    protected $table = 'company_images';

    protected $fillable = [
        'company_id', 'image_name'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

}
