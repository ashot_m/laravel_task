<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyImage;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate(10);
        return view('crud.index', ['companies' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crud.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {

        $number = mt_rand(1000000000, 9999999999);
        $name = $request->get('name');
        $email = $request->get('email');
        $website = $request->get('website');

        $Company = Company::create([
            'name'=> $name,
            'email'=> $email,
            'website' => $website,
            'logo' => '0',
        ]);



        if ($request->file('logo')) {
            $count_file = count($request->file('logo'));
            $Company->logo = $number . $request->file('logo')[0]->getClientOriginalName();
            $Company->save();
            for ($i = 0; $i < $count_file; $i++) {
                Storage::disk('local')->putFileAs('public/', $request->file('logo')[$i], $number . $request->file('logo')[$i]->getClientOriginalName());
                CompanyImage::create([
                    'company_id' => $Company->id,
                    'image_name' => $number . $request->file('logo')[$i]->getClientOriginalName()
                ]);
            }
        }


        return redirect()->route('company.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        $CompanyImages = $company->CompanyImages;
        return view('crud.update', compact(['company', 'CompanyImages']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, $id)
    {

        $company = Company::find($id);
        $company->name = $request->get('name');
        $company->email = $request->get('email');
        $company->website = $request->get('website');
        $count_file = count($request->file('logo'));

        for($i = 0; $i < $count_file; $i++){

            if($request->file('logo') !== null){
                $number = mt_rand(1000000000, 9999999999);
                $logo = $number . $request->file('logo')[$i]->getClientOriginalName();
                $company =  Company::find($id);
                Storage::disk('local')->putFileAs('public/', $request->file('logo')[$i], $logo);
                $company->logo = $logo;
                CompanyImage::create([
                    'company_id' => $company->id,
                    'image_name' => $logo
                ]);
            }

        }

        $company->save();

        return redirect(route('company.index'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company =  Company::find($id);
        Storage::disk('local')->delete('public/'. $company->logo);
        // todo delete images by relation
        $CompanyImage = $company->CompanyImages()->delete();
        foreach ($CompanyImage as $Comp){
            $Comp->delete();
        }
        $company->delete();
        return redirect(route('company.index'));
    }

    public function addProfile(Request $request)
    {
        if($request){
            $CompanyImage = CompanyImage::find($request->get('imgID'));
            $Company = $CompanyImage->Company;
            $Company->logo = $CompanyImage->image_name;
            $Company->save();
            return $CompanyImage->image_name;
        }

    }

    public function deleteImage(Request $request)
    {
        if($request){
            $CompanyImage = CompanyImage::find($request->get('imgID'));
            Storage::disk('local')->delete('public/'. $CompanyImage->image_name);
            $CompanyImage->delete();
            $Company = $CompanyImage->Company;
            $Company->logo = 0;
            $Company->save();
            return $request->get('imgID');
        }

    }
}
