<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\validRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
class CrudController extends Controller
{
    public function index()
    {
        $companies = Company::paginate(10);
        return view('crud.index', ['companies' => $companies]);
    }

    public function create(){
        return view('crud.create');
    }


    public function add(validRequest $request){
        $number = mt_rand(1000000000, 9999999999);
        $name = $request->get('name');
        $email = $request->get('email');
        $website = $request->get('website');
        $logo = $number . $request->file('logo')->getClientOriginalName();

        Company::create([
            'name'=> $name,
            'email'=> $email,
            'website' => $website,
            'logo'=> $logo
        ]);

        Storage::disk('local')->putFileAs('public/',$request->file('logo'), $logo);
        return redirect()->route('crud');
    }

    public function delete($id){
        $company =  Company::find($id);
        Storage::disk('local')->delete('public/'. $company->logo);
        $company->delete();
        return redirect()->route('crud');
    }

    public function update($id)
    {
        $company = Company::find($id);
        return view('crud.update', ['company' => $company]);
    }

    public function up(validRequest $request, $id){
        $number = mt_rand(1000000000, 9999999999);
        if($request->file('logo') !== null){
            $company =  Company::find($id);
            Storage::disk('local')->delete('public/'. $company->logo);
            $logo = $number . $request->file('logo')->getClientOriginalName();
            Storage::disk('local')->putFileAs('public/', $request->file('logo'), $logo);

            DB::table('companies')
                ->where('id', $id)
                ->update([

                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'logo' => $logo,
                    'website' => $request->get('website'),
                ]);
        } else{
            DB::table('companies')
                ->where('id', $id)
                ->update([

                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'website' => $request->get('website'),
                ]);
        }
        return redirect(route('crud'));
    }
}
