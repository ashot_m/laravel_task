<?php

namespace App\Http\Controllers;

use App\Category;
use App\CompanyImage;
use App\Http\Requests\Request;
use App\Product;
use App\Http\Requests\ProductsRequest;
use App\ProductImage;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ProductsList = Product::paginate(10);
        return view('product.index', compact('ProductsList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $SubCategories = Category::whereNotNull('parent_id')->pluck('id', 'name');
        return view('product.create', compact('SubCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductsRequest $request)
    {

        $Number = random_int(1000000, 9000000000);
        $Name = $request->get('name');
        $ImageFiles = $request->file('image');
        $ImageName = $Number . $ImageFiles[0]->getClientOriginalName();
        $Price = $request->get('price');
        $Description = $request->get('short_description');
        $SubCategoryId = $request->get('subcategory_id');

        $Product = Product::create([
            'name' => $Name,
            'image' => '0',
            'price' => $Price,
            'short_description' => $Description,
            'subcategory_id' => $SubCategoryId
        ]);

        if($request->file('image')) {
            $Product->image = $ImageName;
            $Product->save();
            foreach ($ImageFiles as $Image){
                Storage::disk('local')->putFileAs('public/', $Image, $Number . $Image->getClientOriginalName());
                ProductImage::create([
                    'product_id' => $Product->id,
                    'image_name' => $Number . $Image->getClientOriginalName()
                ]);
            }
        }

        return redirect(route('product.index'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Product = Product::find($id);
        $SubCategories = Category::whereNotNull('parent_id')->pluck('id', 'name');
        $ProductImages = $Product->productImages;
        return view('product.update', compact('Product', 'SubCategories', 'ProductImages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

            $Product = Product::find($id);
            $Product->name = $request->get('name');
            $Product->price = $request->get('price');
            $Product->short_description = $request->get('short_description');
            $Product->subcategory_id = $request->get('subcategory_id');


            if($request->file('image') !== null) {
                $Number = random_int(1000000, 9000000000);
                $ImageFiles = $request->file('image');
                foreach ($ImageFiles as $Image) {
                    Storage::disk('local')->putFileAs('public/', $Image, $Number . $Image->getClientOriginalName());
                    ProductImage::create([
                        'product_id' => $Product->id,
                        'image_name' => $Number . $Image->getClientOriginalName()
                    ]);
                }
            }

            $Product->save();

        return redirect(route('product.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Product = Product::find($id);
        $Product->productImages()->delete();
        $Product->delete();
        return redirect(route('product.index'));
    }

    public function addProfile(Request $request)
    {

        if($request->get('imgID')) {
            $ProductImage = ProductImage::find($request->get('imgID'));
            $Product = $ProductImage->products;
            $Product->image = $ProductImage->image_name;
            $Product->save();
            return $ProductImage->image_name;
        }
    }

    public function deleteImage(Request $request)
    {

        if($request->get('imgID')) {
            $ProductImage = ProductImage::find($request->get('imgID'));
            Storage::disk('local')->delete('public/'. $ProductImage->image_name);
            $ProductImage->delete();
            $Product = $ProductImage->products;
            $Product->image = 0;
            $Product->save();
            return $request->get('imgID');
        }

    }
}
