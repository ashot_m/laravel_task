<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo.*' => 'mimes:jpeg,jpg,png',
            'name'=> 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Լրացրեք անուն դաշտը։',
            'logo.*.mimes' => 'Դուք կարող եք ներբեռնել միայն jpeg, jpg, png ֆորմատի ֆայլեր։',
        ];
    }
}
