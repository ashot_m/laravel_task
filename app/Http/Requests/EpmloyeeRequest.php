<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EpmloyeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=> 'required',
            'last_name'=> 'required'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Լրացրեք անուն դաշտը',
            'last_name.required'  => 'Լրացրեք ազգանուն դաշտը',
        ];
    }
}
