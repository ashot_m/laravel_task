<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_images';
    protected $fillable = [
        'product_id', 'image_name'
    ];

    public function products()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
