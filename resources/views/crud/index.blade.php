
<?php

use Illuminate\Support\Facades\Storage;


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <style>
        .my_tr  td, .my_tr  th{
            line-height: 100px;
        }

        .table tr td img{
            width: 100px;
            height: 100px;
        }

        .pagination{
            justify-content: center;
        }

        .nav-item a{
            text-decoration: none;
            color: #000;
            font-size: 35px;
        }

        .nav-item{
            border-right: 1px solid #818182;
        }

        .nav_left{
            background-color: #000000;
        }


        .nav_left a{
            color: #ffffff;
        }

        .modal-body h3{
            margin-bottom: 20px;
        }
    </style>
</head>
<body>


<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal body -->
            <div class="modal-body text-center">
                <h5>Are you sure do you want to delete?</h5>
                <form style="display: inline-block;" class="delete_form" action="" method="POST">
                    <input name="_method" type="hidden" value="DELETE">
                    {{ csrf_field() }}
                    <button  type="submit" class="btn btn-success"  data-toggle="modal" data-target="#myModal">YES</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">NO</button>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>


<ul class="nav nav-tabs nav-justified md-tabs indigo" id="myTabJust" role="tablist">
        <li class="nav-item nav_left">
            <a href="/company">
                    Companies
            </a>
        </li>
        <li class="nav-item">
            <a href="/employee">
                Employees
            </a>
        </li>
        <li class="nav-item">
            <a href="/category">
                Categories
            </a>
        </li>
        <li class="nav-item">
            <a href="/product">
                Products
            </a>
        </li>
    </ul>
    <div class="tab-content card pt-5" id="myTabContentJust">
        <div class="tab-pane fade show active" id="home-just" role="tabpanel" aria-labelledby="home-tab-just">
            <div class="crud_buttons">
                <a href="company/create" class="btn btn-success">Create</a>
            </div>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">logo</th>
                    <th scope="col">Website</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($companies as  $company)
                    <tr class="my_tr">
                        <th scope="row">{{$company->id}}</th>
                        <td>{{$company->name}}</td>
                        <td>{{$company->email}}</td>
                        <td>
                            @if($company->logo === "0")
                                <img src="https://cdn.shopify.com/s/files/1/1486/9104/t/28/assets/no-img.jpg?0" alt="">
                                @else
                                <img src="{{'storage/' . $company->logo}}" alt="">
                            @endif
                            </td>
                        <td>{{$company->website}}</td>
                        <td>
                            <form style="display: inline-block;" action="{{ route('company.destroy' , $company->id)}}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-success del_modal"  data-toggle="modal" data-target="#myModal">X</button>
                            </form>
                            <a href="/company/{{$company->id}}/edit" class="btn btn-success"><i class="far fa-edit"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text_center">
                            {{ $companies->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="profile-just" role="tabpanel" aria-labelledby="profile-tab-just">

        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script>
        $('.del_modal').on('click', function (e) {
            e.preventDefault();
            var form = $(this).parent();
            var form_action = form.attr('action');
            $('.delete_form').attr('action', form_action);
        })
    </script>
</body>
</html>

