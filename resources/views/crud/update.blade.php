<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        .images_box {
            width: 100px;
            height: 100px;
            overflow: hidden;
            margin-right: 15px;
            position: relative;
        }

        .images_box img {
            width: 100%;
            height: 100%;

        }

        .images_block {
            margin-top: 50px;
            display: flex;
            flex-wrap: wrap;
            padding: 30px;
            border: 1px solid #000000;
        }


        .images_box a{
            padding: 5px;
            background-color: rgba(0,0,0,.4);
            position: absolute;
            right: 0;
            top: 0;
            color: #ffffff;
        }

        .modal-body form {
            display: inline-block;
        }
    </style>


</head>
<body>

<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal body -->
            <div class="modal-body">
                <form action="">
                    @csrf
                    {{ method_field('POST') }}
                    <input type="hidden">
                    <button class="btn btn-success" id="delete_img">Delete</button>
                </form>

                <form action="" method="post">
                    @csrf
                    {{ method_field('POST') }}
                    <input type="hidden" name="logo">
                    <button class="btn btn-danger" id="add_img">Add Profile Photo</button>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
@if($errors->any())
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-2">
                <ul>
                    @foreach($errors->all() as $error)
                        <li style="color: red;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <div class="images_box prof_img">
                @if($company->logo === '0')
                    <img src="https://cdn.shopify.com/s/files/1/1486/9104/t/28/assets/no-img.jpg?0" alt="">
                    @else
                    <img src="../../storage/{{$company->logo}}" alt="">
                    @endif


            </div>
        </div>
        <div class="col-md-10">
            <form action="{{route('company.update',$company->id )}}" enctype="multipart/form-data" method="post">
                @csrf
                @method('POST')
                {{ method_field('PUT') }}
                <div class="form_group">
                    <input type="text" placeholder="Name" class="form-control" name="name" value="{{$company->name}}">
                    <input type="email" placeholder="Email" class="form-control" name="email" value="{{$company->email}}">
                    <input type="text" placeholder="Website" class="form-control" name="website" value="{{$company->website}}">
                    <input type="file" name="logo[]" multiple>
                    <button class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
    <div class="images_block">
        @if($CompanyImages)
            @php
                $ImgCount = count($CompanyImages);
            @endphp
            @for($i = 0; $i < $ImgCount; $i++)
                <div class="images_box" data-ImgID="{{$CompanyImages[$i]->id}}">
                    <a href="" class="img_val" data-ImgID="{{$CompanyImages[$i]->id}}"  data-toggle="modal" data-target="#myModal">
                        <i class="far fa-edit"></i>
                    </a>
                    <img src="../../storage/{{$CompanyImages[$i]->image_name}}" alt="">
                </div>
            @endfor
        @endif
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script>
    $('.img_val').on('click', function () {

        var val = $(this).data('imgid');
        $('.modal-body form input').val(val);

    })
</script>

<script>
    $(document).ready(function(e){
        $('#add_img').click(function(){
            $imgID = $('.modal-body form input').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            jQuery.ajax({
                url: '/company/addProfile',
                type: 'post',
                data:{
                    'imgID':$imgID
                },
                success:function(data){
                    if (data){
                        $('.prof_img img').attr('src', '../../storage/'+ data )
                        $('body').removeClass("modal-open")
                        $('.modal-backdrop').removeClass('modal-backdrop');
                        $('#myModal').removeClass('show').css('display', 'none');
                    }
                    console.log(data);
                },
                error: function (xhr, b, c) {
                    console.log('error AddProfileImage');
                }
            });
            return false;
        });



        $('#delete_img').click(function(){

            $imgID = $('.modal-body form input').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            jQuery.ajax({
                url: '/company/deleteImage',
                type: 'post',
                data:{
                    'imgID':$imgID
                },
                success:function(data){
                    if (data){
                        console.log(data);
                        $('.images_box').each(function () {
                            if( parseInt(data)  === $(this).data('imgid')){
                                $(this).remove();
                            }
                        })
                        $('body').removeClass("modal-open")
                        $('.modal-backdrop').removeClass('modal-backdrop');
                        $('#myModal').removeClass('show').css('display', 'none');
                    }
                },
                error: function () {
                    console.log('error DeleteImage');
                }
            });
            return false;
        });
    });
</script>

</body>
</html>
