<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

@if($errors->any())
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-2">
                <ul>
                    @foreach($errors->all() as $error)
                        <li style="color: red;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-2">
            <form action="{{route('employee.update', $employee->id)}}" method="post">
                @csrf
                {{ method_field('PUT') }}
                <div class="form_group">
                    <input type="text" placeholder="First Name" class="form-control" name="first_name" value="{{$employee->first_name}}">
                    <input type="text" placeholder="Last Name" class="form-control" name="last_name" value="{{$employee->last_name}}">
                    <select name="company_id" id="" class="form-control" >
                        @foreach($companies as $company => $value)
                            <option @if ($employee->id == $value) selected @endif   value="{{$value}}">{{$company}}</option>
                        @endforeach
                    </select>
                    <input type="email" name="email" placeholder="Email" class="form-control" value="{{$employee->email}}">
                    <input type="text" name="phone" placeholder="+37477000000" class="form-control" value="{{$employee->phone}}">
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>

